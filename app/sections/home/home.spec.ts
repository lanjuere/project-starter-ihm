import { AbstractTest } from "./../../beans/tests/AbstractTest";
import { HomeComponent } from "./home.component";

export class HomeSpec extends AbstractTest{
    run (){
        
        describe('Home Page', () => {
           var homeComponent = new HomeComponent();
            it('Message is well initiate', () => expect(homeComponent.message).toEqual("message"));
            it('PlaceHoled is message', () => expect(homeComponent.placeholder).toEqual("message"));
        });
    }
}