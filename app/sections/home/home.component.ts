import { Component } from '@angular/core';
@Component({
  moduleId : module.id,
  selector: 'home',
  templateUrl: 'home.html'
})
 

export class HomeComponent { 

  titre = "page d'accueil";
  message = "message";
  placeholder = "message";
  keepMessage = function($event){
    this.message = this.tmp;
    this.tmp = undefined;
  }
}
