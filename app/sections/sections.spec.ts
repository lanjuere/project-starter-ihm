import { HomeSpec } from './home/home.spec';
import { AbstractTestModule } from "./../beans/tests/AbstractTestModule";


export class SectionsTestsRunner extends AbstractTestModule{

    constructor(){
        super();
        this.items.push(new HomeSpec());
    }
}