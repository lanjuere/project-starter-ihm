import { AbstractTestModule } from './beans/tests/AbstractTestModule';
import { SectionsTestsRunner } from './sections/sections.spec';



class MainTestRunner extends AbstractTestModule {
    constructor(){
        super();
        this.items.push(new SectionsTestsRunner());
    }
}

var mainTestRunner = new MainTestRunner();
mainTestRunner.run();

