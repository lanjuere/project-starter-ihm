import { AbstractTest } from "./AbstractTest";


export abstract class AbstractTestModule extends AbstractTest{

    protected items : Array<AbstractTest> = new Array<AbstractTest>(); 

    run(){
        for(var item of this.items){
            item.run();
        }
    }

}